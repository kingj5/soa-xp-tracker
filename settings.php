<?php
session_start();
if(!isset($_SESSION['is_auth']))
{
	session_destroy();
	header ( "LOCATION: ./login.php" );
	exit;
}
else if(!$_SESSION['is_auth'])
{
	session_destroy();
	header ( "LOCATION: ./login.php" );
	exit;
}

include("./inc/templates.php");
require("./inc/dbfuncs.php");
include("./comp/compapi.php");

/*$conn = dbconn();

$query = $conn->query("
		SELECT *
		FROM settings
		");

$resulttype = MYSQLI_ASSOC;
$getsettings = mysqli_fetch_array($query, $resulttype);*/
?>

<!DOCTYPE html>
<html>
<head>
	<title>Modify the site settings</title>
<?= $headerinclude = template("headerinclude");?>

</head>
<body id="competition--edit">
	<?= $header = template("header"); ?>
	<section class="competition-body">
		<h3 class="page-title">Modify Admin Settings</h3>
	<?php 
	if (isset($_GET['error']) && $_GET['error'] == "invalidpass") 
	{
		echo "<p class=\"competition-error\">Passwords do not match; please check and try again.</p>";
	}
	if (isset($_GET['error']) && $_GET['error'] == "incorrectpass")
	{
		echo "<p class=\"competition-error\">Incorrect password; try again.</p>";
	}
	if (isset($_GET['action']) && $_GET['action'] == "complete")
	{
		echo "<p class=\"competition-error\">Settings Changed.</p>";
	}
	if (isset($_GET['error']) && $_GET['error'] == "samepass")
	{
		echo "<p class=\"competition-error\">You cannot set a new password to be the same as the old one.</p>";
	}
	?>
		<form action="settings.php" method="post">
			<p>Enter current password: <input id="oldpass" type="password" name="currpass" required> <a class="pass-hide" href="#oldpass" onclick="revealpass('oldpass', 'oldhide');">(<span id="oldhide">Show</span> Password)</a></p>
			<p>Set new admin password: <input id="newpass" type="password" name="newpass" required> <a class="pass-hide" href="#newpass" onclick="revealpass('newpass', 'newhide');">(<span id="newhide">Show</span> Password)</a></p>
			<p>Re-Enter new password: <input type="password" name="confirm" required></p>
			<input type="submit" name="submit" value="Submit" class="submit-button">
		</form>
	</section>
	<script type="text/javascript">
		function revealpass(id, val2) {
			var inputnode = document.getElementById(id);
			var togglenode = document.getElementById(val2);
			if (inputnode.type == "password") {
				inputnode.type = "text";
				togglenode.childNodes[0].nodeValue = "Hide";
			}
			else {
				inputnode.type = "password";
				togglenode.childNodes[0].nodeValue = "Show";
			}
		}
	</script>
</body>
</html>
<?php 

	if (isset($_POST['submit']) && $_POST['submit'] == "Submit") {
		$conn = dbconn();
		$query = "select * from settings where setname = \"adminpass\"";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$temppass = mysqli_escape_string($conn, $_POST['currpass']);
	if (password_verify($temppass, $row['setval'])) {
				$passmatch = true;
		}
		
		
		$newpass = $_POST['newpass'];
		$confirmnew = $_POST['confirm'];
		if (!$passmatch) {
			header("LOCATION: ./settings.php?error=incorrectpass");
		}
		else {
			if ($newpass != $confirmnew) {
				header("LOCATION: ./settings.php?error=invalidpass");
			}
			else {
				if ($newpass == $_POST['currpass']) {
					header("LOCATION: ./settings.php?error=samepass");
				}
				else {
					//put the updating database stuff here!
					$passoptarray = [
							'cost' => 12,
					];
					$setnewpass = password_hash($newpass, PASSWORD_DEFAULT, $passoptarray);
					$updatequery="UPDATE settings SET setval='".$setnewpass."' WHERE setname='adminpass'";
					$conn->query($updatequery);
					
					header("LOCATION: ./settings.php?action=complete");
				}
			}
		}
		
		
		
		$conn->close();
		
	}

?>