<?php 
// session_start();


/*
$currentcomp = 1;
if ($currentcomp == 1) {
$nocompetitions = '<p class="competition-error">There aren\'t any active competitions running at this moment</p>';
}
else {
	$nocompetitions = "";
}*/

$passfile = basename($_SERVER['PHP_SELF']);
$passquery = $_SERVER['QUERY_STRING'];

?>


<header class="competition-header" id="competition-banner">
	<div class="competition-header__options">
	 <?php
// 	 $loggedin = isset($_SESSION['is_auth']);
// 	 if ($loggedin == true) {
// 	 	echo '<a href="login.php?action=logout&logout=dologout">Log Out</a>';
// 	 }
// 	 else {
// 	 	echo '<a href="login.php">Log In</a>';
// 	 }	 
	 ?>
	</div>
	<div class="competition-header__banner">
		<h3 class="competition-header__name"><a href="./index.php"><img class="competition-header__image" src="./template/images/soa_generic_forumlogo.png" title="Spirits of Arianwyn" alt="Spirits of Arianwyn"></a></h3>
		<nav class="site-navigation--container">
			<ul class="site-navigation">
				<li class="nav-item nav-item--1"><a href="http://forums.soa-rs.com"><i class="fas fa-comment-alt"></i> SoA Forums</a></li>
				<li class="nav-item nav-item--2"><a href="./index.php"><i class="fas fa-list-alt"></i> XP Comps</a></li>
				<?php
				$loggedin = isset($_SESSION['is_auth']);
				if ($loggedin == true) {
					echo "
						  <li class=\"nav-item nav-item--3\"><a href=\"./compadd.php\"><i class=\"fas fa-calendar-plus\"></i> Start New comp</a></li>
						  <li class=\"nav-item nav-item--4\"><a href=\"./settings.php\"><i class=\"fas fa-cogs\"></i> Settings</a></li>
						  <li class=\"nav-item nav-item--5\"><a href=\"login.php?action=logout&logout=dologout\"><i class=\"fas fa-sign-out-alt\"></i> Logout</a></li>";
				}
				else {
					echo "<li class=\"nav-item nav-item--3\"><a href=\"login.php?dest={$passfile}&params={$passquery}\"><i class=\"fas fa-sign-in-alt\"></i> Admin Login</a>";
				}
				?>
				
			</ul>
		</nav>
	</div>
</header>