<?php
// include("./inc/templates.php");
?>



<!DOCTYPE html>
<html>
<head>
<title>Signing Out</title>
<?= $headerinclude = template("headerinclude");?>
</head>
<body id="competitions--login">
	<?= $header = template("header"); ?>
	<section class="competition-body">
		<h3 class="page-title">You have logged out</h3>
		<p>You have successfully signed out of your account.</p>
		<p><a href="./index.php" class="competition-data__item-nameLink">Click here to return to the main page.</a></p>
		</section>
			<?= $footer = template("footer"); ?>

</body>
</html>
		