<?php
date_default_timezone_set('UTC');
session_start();
if(!isset($_SESSION['is_auth']))
{
	session_destroy();
	header ( "LOCATION: ./login.php" );
	exit;
}
else if(!$_SESSION['is_auth'])
{
	session_destroy();
	header ( "LOCATION: ./login.php" );
	exit;
}

include("./inc/templates.php");
require("./inc/dbfuncs.php");
include("./comp/compapi.php");

$compid = null;

if (isset($_GET['compid']))
{
	$compid = $_GET['compid'];
}
else if (isset($_POST['compid']))
{
	$compid=$_POST['compid'];
}
else 
{
	$errortext = "Error: Competition ID must be specified.";
	$error = true;
}

if(isset($_POST['actionButton']) && $_POST['actionButton'] == "Edit Competition") 
{
	header ( "LOCATION: ./editcomp.php?compid=".$compid); //This kills any post request from the Edit Button on viewcomp.
}
if(isset($_GET['error']) && $_GET['error'] == "name")
{
	$error=true;
	$errortext = "Error with name change";
}

$conn = dbconn();

$query = $conn->query(" 
		SELECT *
		FROM competitions
		WHERE compid = '$compid'
		");

$resulttype = MYSQLI_ASSOC;
$getcomp = mysqli_fetch_array($query, $resulttype);

$compname = $getcomp['compname'];
$skill = $getcomp['skill'];
$starttimes = $getcomp['starttime'];
$endtimes = $getcomp['endtime'];
$privacy = $getcomp['privacy'];
$status = $getcomp['status'];

$checked = "";
if ($privacy == 1) 
{
	$checked = "checked=\"checked\"";
}

$epoch = $starttimes;
$starttime =  date('Y/m/d H:i', $epoch);

$epoch2 = $endtimes;
$endtime =  date('Y/m/d H:i', $epoch2);

$participantlist = $conn->query("
		SELECT player
		FROM participants
		WHERE compid = '$compid'
		order by player asc
		");

$participantsstring=null;
$participantsarray = array();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Modify an existing competition</title>
<?= $headerinclude = template("headerinclude");?>
</head>
<body id="competition--edit">
	<?= $header = template("header"); ?>
	<section class="competition-body">
		<h3 class="page-title">Edit a Competition</h3>
		<?php 
	if (isset($_GET['error']) && $_GET['error'] == "true") 
	{
		echo "<p class=\"competition-error\">Check date; cannot have competition start before today or end before start time</p>";
	}
	else if (isset($error))
	{
		echo "<p class=\"competition-error\">".$errortext."</p>";
		if(strpos($errortext, "Competition ID") !== false)
		{
			echo"</section>
			".template("footer")."</body>";
			exit;
		}
	}
	?>
		<form action="?action=do_edit" method="post" style="width:100%">
		<?php echo "<input type=\"hidden\" name=\"compid\" value=\"".$compid."\">\n"?>
			<div class="competition-body__body">
				<div class="competition-options--column-1">
					<label class="competition-option--add">Competition Name: <input class="comp-input" name="compname" type="text" placeholder="Example: Soa July Hunter" required value="<?= $compname; ?>"></label>
					<label class="competition-option--add">Add New Participants:<br> <textarea class="comp-input" name="members" placeholder="Put names here, new line for each one" rows="10" cols="15"></textarea></label>
					<label class="competition-option--add">Remove Participant:<br> <select class="comp-input" name="removemembers[]" style="height: 150px; width: 150px;" multiple><?= listParticipants(); ?></select></label>
				</div>
				<div class="competition-options--column-2">
					<label class="competition-option--add">Start Date: <input class="comp-input" id="date-time__picker--start" type="text" name="datestart" value="<?= $starttime; ?>" required></label>
					<label class="competition-option--add">End Date: <input class="comp-input" id="date-time__picker--end" type="text" name="dateend" value="<?= $endtime; ?>" required></label>
					<label class="competition-option--add"><input type="checkbox" name="private" <?= $checked; ?>> Make the competition private.</label>
					<div class="competition-submit"><input id="competition-submit" type="submit" value="Save"><input id="competition-cancel" type="button" value="Cancel" onclick="window.location='./viewcomp.php?compid=<?php echo $compid; ?>'";></div>
				</div>
			</div>
		</form>
		<hr style="border-color: #5C5C5C;">
		<form action="?action=namechange" method="post" style="padding: 20px;">
		<h3 class="page-title">Change Participant Names</h3>
		<?php echo "<input type=\"hidden\" name=\"compid\" value=\"".$compid."\">\n"?>
		<label class="competition-option--add">Current Name: <select name="currentmember" style="width: 150px;"><option value="">-Select a name-</option><?= $participantsstring; ?></select> 
		<label class="competition-option--add">New Name: <input class="comp-input" type="text" placeholder="New name here" name="newname" required></label>
		<input id="competition-submit" type="submit" value="Save"></form>
	</section>

	<?= $footer = template("footer");?>

</body>
<!-- this should go after your </body> -->
<link rel="stylesheet" type="text/css" href="./inc/datetimepicker-master/jquery.datetimepicker.css"/ >
<script src="./inc/datetimepicker-master/jquery.js"></script>
<script src="./inc/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
<script>
jQuery('#date-time__picker--start').datetimepicker();
jQuery('#date-time__picker--end').datetimepicker();
</script>
</html>

<?php
if(isset($_GET['action']))
{
	$action = $_GET ['action'];
}
if (isset ( $action ) && $action == "do_edit") 
{
	
	$name = mysqli_real_escape_string($conn, $_POST['compname']);
	$members = mysqli_real_escape_string($conn, $_POST['members']);
	$datestart = mysqli_real_escape_string($conn, $_POST['datestart']);
	$dateend = mysqli_real_escape_string($conn, $_POST['dateend']);
	
	if ($_POST['private'] == "on") 
	{
		$privatestatus = "1";
	}
	else 
	{
		$privatestatus = "0";
	}

	$datestart = str_replace("%2F","/",$datestart,$i);
	$datestart = str_replace("%3A", ":", $datestart,$i);
	
	$dateend = str_replace("%2F","/",$dateend,$i);
	$dateend = str_replace("%3A", ":", $dateend,$i);
	
	$current_time = time();
	
	$convertstart = new DateTime($datestart);
	$new_date_start = $convertstart->format("U");
	$convertend = new DateTime($dateend);
	$new_date_end = $convertend->format("U");
	
	if ($new_date_start <= $current_time && $status == 0) 
	{
		exit_redirect("./editcomp.php?compid=".$compid."&error=true");
	}
	if ($new_date_end <= $new_date_start) {
		exit_redirect("./editcomp.php?compid=".$compid."&error=true");
	}
	
	if ($status > 0)
	{
		$new_date_start == $starttimes;
	}
	
	$query = "UPDATE competitions set compname=\"".$name."\", starttime=\"".$new_date_start."\", endtime=\"".$new_date_end."\", updatetime=\"".$current_time."\", privacy=\"".$privatestatus."\" WHERE compid='$compid'";
	$conn->query($query);
	
	if($members != "")
	{
		$memberarray = explode("\\r\\n", $members);
		$memberarray = array_map('trim', $memberarray);
		$memberarray = str_replace(" ", "_", $memberarray);
		$memberarray = array_iunique($memberarray);
		foreach ($memberarray as $newmember) 
		{
			if(!i_in_array($newmember, $participantsarray) && trim($newmember) != "")
			{
				addPlayer($conn, $compid, $newmember);
			}
		}
		startBackgroundEdit($compid, "edit", $memberarray);
	}
	
	foreach ($_POST['removemembers'] as $removemember)
	{
		$query= "delete from participants where player='$removemember' and compid=$compid";
		$conn->query($query);
	}
	
	$url="./editcomp.php?compid=".$compid;
	exit_redirect($url);
}

else if (isset ( $action ) && $action == "namechange")
{
	$originalname = mysqli_real_escape_string($conn, $_POST['currentmember']);
	$newname= mysqli_real_escape_string($conn, $_POST['newname']);
	
	$query="select endxp, id from participants where compid=$compid and player='$originalname'";
	$result = $conn->query($query);
	
	$row=$result->fetch_assoc();
	$originalxp = $row['endxp'];
	$id=$row['id'];
	
	$array = getXpAndLevel($newname, $skill);
	if($array != null)
	{
		if ($originalxp <= $array[1])
		{
			$query="update participants set player='$newname' where id=$id";
			$conn->query($query);
			$url="./editcomp.php?compid=".$compid;
			exit_redirect($url);
		}
		else 
		{
			exit_redirect("./editcomp.php?compid=".$compid."&error=name");
		}
	}
	else 
	{
		exit_redirect("./editcomp.php?compid=".$compid."&error=name");
	}
}

function listParticipants()
{
	global $participantlist, $participantsstring, $participantsarray;
	$rows = $participantlist->num_rows;
	for ($i = 0; $i < $rows; $i++)
	{
		$row = $participantlist->fetch_assoc();
		$participantsstring = $participantsstring. "<option value=\"".$row['player']."\">".$row['player']."</option>
		";
		array_push($participantsarray, $row['player']);
	}
	$participantsarray = str_replace(" ", "_", $participantsarray);
	echo $participantsstring;

}



function array_iunique($array) 
{
	return array_intersect_key($array, array_unique(array_map("StrToLower",$array)));
}

function i_in_array($needle, $haystack)
{
	return in_array(strtolower($needle), array_map("strtolower", $haystack));
}

$conn->close();

function exit_redirect($url)
{
	die('<script type="text/javascript">window.location=\''.$url.'\';</script>');
}
?>
