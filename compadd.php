<?php
date_default_timezone_set('UTC');
session_start();
if(!isset($_SESSION['is_auth']))
{
	session_destroy();
	header ( "LOCATION: ./login.php" );
	exit;
}
else if(!$_SESSION['is_auth'])
{
	session_destroy();
	header ( "LOCATION: ./login.php" );
	exit;
}

include("./inc/templates.php");
require("./inc/dbfuncs.php");
include("./comp/compapi.php");
$skills = null;

$skilllist = array (
			"Overall",
			"Attack",
			"Defence",
			"Strength",
			"Constitution",
			"Ranged",
			"Prayer",
			"Magic",
			"Cooking",
			"Woodcutting",
			"Fletching",
			"Fishing",
			"Firemaking",
			"Crafting",
			"Smithing",
			"Mining",
			"Herblore",
			"Agility",
			"Thieving",
			"Slayer",
			"Farming",
			"Runecrafting",
			"Hunter",
			"Construction",
			"Summoning",
			"Dungeoneering",
			"Divination",
			"Invention" 
	);

	foreach($skilllist as $skillname) {
		$skills = $skills.'<option value="'.$skillname.'">'.$skillname.'</option>';
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Add a new Clan Competition</title>
<?= $headerinclude = template("headerinclude");?>
</head>
<body id="competitions--edit">
	<?= $header = template("header"); ?>
	<?php 
	if (isset($_GET['error']) && $_GET['error'] == "true") {
		echo "<p class=\"competition-error\">Check date; cannot have competition start before today or end before start time</p>";
	}
	?>
	<section class="competition-body">
		<h3 class="page-title">Add a New Competition</h3>
		<form action="?action=addcomp" method="post">
			<div class="competition-body__body">
				<div class="competition-options--column-1">
					<label class="competition-option--add">Competition Name: <input class="comp-input" name="compname" type="text" placeholder="Example: Soa July Hunter" required autofocus></label>
					<label class="competition-option--add">Tracking Skill: <select class="comp-select" name="skill" required><option value="" disabled selected>Select a skill</option><?= $skills ?></select></label>
					<label class="competition-option--add">Participants:<br> <textarea class="comp-input" name="members" placeholder="Put names here, new line for each one" rows="10" cols="15" required></textarea></label>
				</div>
				<div class="competition-options--column-2">
					<label class="competition-option--add">Start Date: <input class="comp-input" id="date-time__picker--start" type="text" name="datestart" required></label>
					<label class="competition-option--add">End Date: <input class="comp-input" id="date-time__picker--end" type="text" name="dateend" required></label>
					<label class="competition-option--add"><input type="checkbox" name="private"> Make the competition private.</label>
					<div class="competition-submit"><input id="competition-submit" type="submit" value="Save"><input id="competition-cancel" type="button" value="Cancel" onclick="window.location='./index.php'"></div>
				</div>
			</div>
		</form>
	</section>

	<?= $footer = template("footer");?>

</body>
<!-- this should go after your </body> -->
<link rel="stylesheet" type="text/css" href="./inc/datetimepicker-master/jquery.datetimepicker.css"/ >
<script src="./inc/datetimepicker-master/jquery.js"></script>
<script src="./inc/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
<script>
jQuery('#date-time__picker--start').datetimepicker();
jQuery('#date-time__picker--end').datetimepicker();
</script>
</html>

<?php
if(isset($_GET['action']))
{
	$action = $_GET ['action'];
}
if (isset ( $action ) && $action == "addcomp") {
	
	$conn = dbconn();
	
	$name = mysqli_real_escape_string($conn, $_POST['compname']);
	$skillid = $_POST['skill'];
	$members = mysqli_real_escape_string($conn, $_POST['members']);
	$poststart = mysqli_real_escape_string($conn, $_POST['datestart']);
	$postend = mysqli_real_escape_string($conn, $_POST['dateend']);
	if(isset($_POST['private']))
	{
		$isprivate = $_POST['private'];
	}
	else {
		$isprivate = "off";
	}
	
	if ($isprivate == "on") {
		$privatestatus = "1";
	}
	else {
		$privatestatus = "0";
	}

	$olddate = $poststart;
	$olddate2 = str_replace("%2F","/",$olddate,$i);
	$datestart = str_replace("%3A", ":", $olddate2,$i);
	
	$olddate3 = $postend;
	$olddate4 = str_replace("%2F","/",$olddate3,$i);
	$dateend = str_replace("%3A", ":", $olddate4,$i);
	
	$current_time = time();
	
	$convertstart = new DateTime($datestart);
	$new_date_start = $convertstart->format("U");
	$convertend = new DateTime($dateend);
	$new_date_end = $convertend->format("U");
	
	if ($new_date_start <= $current_time) {
		exit_redirect("./compadd.php?error=true");
	}
	if ($new_date_end <= $new_date_start) {
		exit_redirect("./compadd.php?error=true");
	}
	
	$comparray = array(
	 "compname" => $name,
	 "skill" => $skillid,
	 "status" => "0",
     "starttime" => $new_date_start,
	 "endtime" => $new_date_end,
	 "updatetime" => $current_time,
	 "privacy" => $privatestatus
	);
	
	$insertarray =  "'" . implode("','", $comparray) . "'";
	$query = "INSERT INTO competitions (compname, skill, status, starttime, endtime, updatetime, privacy) VALUES ($insertarray)";
	$conn->query($query);
	
	$tableid = $conn->insert_id;
	
	$memberarray = explode("\\r\\n", $members);
	$memberarray = array_map('trim', $memberarray);
	$memberarray = str_replace(" ", "_", $memberarray);
	$memberarray = array_iunique($memberarray); //remove duplicates
	foreach ($memberarray as $newmember) {
		if(trim($newmember) != "")
		{
			addPlayer($conn, $tableid, $newmember);
		}
	}
	$conn->close();
	
	startBackgroundUpdate($tableid, "add");
	
	$url="./viewcomp.php?compid=".$tableid;
	exit_redirect($url);

}

function array_iunique($array) 
{
	return array_intersect_key($array, array_unique(array_map("StrToLower",$array)));
}

function exit_redirect($url)
{
	die('<script type="text/javascript">window.location=\''.$url.'\';</script>');
}
?>
