<!DOCTYPE html>
<html>
<head>
<title>SoA Competition Tracker - Database Setup</title>
</head>
<body>
<h1>SoA Competition Tracker - Database Creation - Step 1</h1>

<?php

if(!file_exists('../inc/config.php'))
{
	echo "The 'config.php' file does not exist. <br>Please create a blank file at 'inc/config.php' and give it 777 permissions.";
	exit;
}

//Check if config file is writable.  If not, produce an error
if(!is_writable('../inc/config.php'))
{
	echo "The 'config.php' file is not writable.<br>Please execute 'chmod 777 inc/config.php' in the root directory of this repo to allow the script to write to this file.";
	exit;
}

?>
<p>Please enter your database information below so that we may set up your tables.</p>

<form action="install2.php" method="post">
<br>Database Host: <input type="text" name="dbhost" required>
<br>Database Username: <input type="text" name="dbuser" required>
<br>Database User Password: <input type="password" name="dbpass" required>
<br>Database Name: <input type="text" name="dbname" required>
<br>Admin Password to access comps: <input type="password" name="adminpass" required>
<br>Confirm Admin Password: <input type="password" name="adminpass2" required>
<br><input type="submit" value="Create Tables">
</form>
<br><br>
<p>Please note - by clicking submit, any previous table information and previous configuration files will be overwritten.</p>

</body></html>