<?php
// ini_set ( "max_execution_time", 0 ); // TODO - Investigate
date_default_timezone_set ( 'UTC' );
function getHTML($url) {
	$ch = curl_init ( $url ); // initialize curl with given url
	curl_setopt ( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0" ); // set useragent
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true ); // write the response to a variable
	curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true ); // follow redirects if any
	curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 15 ); // max. seconds to execute
	curl_setopt ( $ch, CURLOPT_TIMEOUT, 10 ); // limit each attempt to 10 seconds
	curl_setopt ( $ch, CURLOPT_FAILONERROR, 1 ); // stop when it encounters an error
	return @curl_exec ( $ch );
}

/**
 * Get the Current XP and Level for a specific player in a specific skill from the RuneScape Lite Hiscores.
 *
 * @param $player -
 *        	The name of the player
 * @param $skill -
 *        	The name of the skill, in text form
 * @param $retry -
 *        	Retry boolean; if true and a request to Jagex's API fails, the function will attempt once more to get stats before giving up.
 * @return reqdata - An array of the requested data, in the form of $reqdata[0] = level, $reqdata[1] = xp
 */
function getXpAndLevel($player, $skill, $retry) {
	// $hs = file_get_contents ( "http://hiscore.runescape.com/index_lite.ws?player=" . $player );
	$hs = getHTML ( "http://services.runescape.com/m=hiscore/index_lite.ws?player=" . $player );
	
	$hs = explode ( "\n", $hs );
	
	$skills = array (
			"Overall",
			"Attack",
			"Defence",
			"Strength",
			"Constitution",
			"Ranged",
			"Prayer",
			"Magic",
			"Cooking",
			"Woodcutting",
			"Fletching",
			"Fishing",
			"Firemaking",
			"Crafting",
			"Smithing",
			"Mining",
			"Herblore",
			"Agility",
			"Thieving",
			"Slayer",
			"Farming",
			"Runecrafting",
			"Hunter",
			"Construction",
			"Summoning",
			"Dungeoneering",
			"Divination",
			"Invention" 
	);
	
	$i = 0;
	
	foreach ( $skills as $value ) {
		
		if (isset ( $hs [$i] )) {
			$hs [$i] = explode ( ",", $hs [$i] );
			$stats [$value] ["rank"] = $hs [$i] [0];
			if (isset ( $hs [$i] [1] ) && isset( $hs [$i] [2])) {
				$stats [$value] ["level"] = $hs [$i] [1];
				$stats [$value] ["xp"] = $hs [$i] [2];
			}
			else {
				if ($retry) {
					getXpAndLevel ( $player, $skill, false );
				}
			}
			$i ++;
		}
	}
	
	if (isset ( $stats [$skill] ["level"] )) {
		$reqdata = array (
				$stats [$skill] ["level"],
				$stats [$skill] ["xp"] 
		);
		return $reqdata;
	} 

	else
		return null;
}
/**
 * Return the full list of participants in a competition, ordered by their xp gain (most xp gained at the top).
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 * @return $result - The result set. If null is returned, it means the data was not able to be fetched.
 */
function getCompetitionParticipants($conn, $compid) {
	$query = "select * from participants where compid = '" . $compid . "' order by xpgained desc";
	$result = $conn->query ( $query );
	if (! $result) {
		return null;
	} else {
		return $result;
	}
}
/**
 * Set a player's current xp in a competition.
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 * @param $player -
 *        	The player's name
 * @param $xp -
 *        	The current XP of the player.
 * @return boolean - Success Status boolean
 */
function setCurrentXP($conn, $compid, $player, $xp) {
	$query = "update participants set endxp=\"" . $xp . "\" where compid = \"" . $compid . "\" and player = \"" . $player . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}
/**
 * Set a player's current level in a competition.
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 * @param $player -
 *        	The player's name
 * @param $lvl -
 *        	The current level of the player.
 * @return boolean - Success Status boolean
 */
function setCurrentLevel($conn, $compid, $player, $lvl) {
	$query = "update participants set endlvl=\"" . $lvl . "\" where compid = \"" . $compid . "\" and player = \"" . $player . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}
/**
 * Remove a participant from a competition.
 *
 * @param $conn -
 *        	Database Connection Object.
 * @param $compid -
 *        	The ID of the competition
 * @param $player -
 *        	The player's name.
 * @return boolean - Success Status boolean
 */
function removeParticipant($conn, $compid, $player) {
	$query = "delete from participants where compid = \"" . $compid . "\" and player = \"" . $player . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}
/**
 * Set the privacy of a competition: 0 is public, 1 is private
 *
 * @param $conn -
 *        	Database Connection Object.
 * @param $compid -
 *        	The ID of the competition
 * @param $privacy -
 *        	The privacy setting.
 * @return boolean - Success Status boolean
 */
function setPrivacy($conn, $compid, $privacy) {
	$query = "update competitions set privacy = \"" . $privacy . "\" where compid = \"" . $compid . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}

/**
 * Get the skill being tracked for a competition.
 *
 * @param $conn -
 *        	Database Connection Object.
 * @param $compid -
 *        	The ID of the competition
 * @return The skill of the competition. If null is returned, then the query was unsuccessful.
 */
function getSkill($conn, $compid) {
	$query = "select skill from competitions where compid = " . $compid;
	$result = $conn->query ( $query );
	if (! $result) {
		return null;
	} else {
		$row = $result->fetch_assoc ();
		return $row ['skill'];
	}
}

/**
 * Returns a Competition Name
 *
 * @param $conn -
 *        	Database Connection Object.
 * @param $compid -
 *        	The ID of the competition
 * @return The name of the competition. If null is returned, its invalid.
 */
function getCompName($conn, $compid) {
	$query = "select compname from competitions where compid = \"" . $compid . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return null;
	} else {
		$row = $result->fetch_assoc ();
		return $row ['compname'];
	}
}
/**
 * Add a player to a competition. This adds the player with blank stats,
 * to be captured later in a background update.
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 * @param $player -
 *        	The player's name
 * @return boolean - Success Status boolean
 */
function addPlayer($conn, $compid, $player) {
	// $skill = getSkill ( $conn, $compid );
	// $stats = getXpAndLevel ( $player, $skill );
	// $level = $stats [0];
	// $skill = $stats [1];
	$query = "insert into participants (player, compid, startxp, startlvl, endxp, endlvl, xpgained, lvlgained) values ('$player','$compid', '0', '0', '0', '0', '0', '0')";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}

/**
 * Starts a competition
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 * @param $time -
 *        	The current time
 */
function startComp($conn, $compid) {
	$skill = getSkill ( $conn, $compid );
	$result = getCompetitionParticipants ( $conn, $compid );
	$playercount = $result->num_rows;
	
	for($i = 0; $i < $playercount; $i ++) {
		$playerrow = $result->fetch_assoc ();
		$player = $playerrow ['player'];
		$currentstats = getXpAndLevel ( $player, $skill, true );
		if ($currentstats == null) {
			initializePlayer ( $conn, $playerrow ['id'], 0, 0 );
		} else {
			initializePlayer ( $conn, $playerrow ['id'], $currentstats [1], $currentstats [0] );
		}
	}
}

/**
 * Sets a player's starting xp and level.
 * * @param $conn -
 * Database connection object
 *
 * @param $id -
 *        	The player's ID within the participants table.
 * @param $curxp -
 *        	The player's current xp
 * @param $curlvl -
 *        	The player's current level.
 * @return boolean - Success Status boolean
 */
function initializePlayer($conn, $id, $curxp, $curlvl) {
	$query = "update participants set startxp = \"" . $curxp . "\", startlvl = \"" . $curlvl . "\", endxp = \"" . $curxp . "\", endlvl = \"" . $curlvl . "\" where id = \"" . $id . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}
/**
 * Update all players within a competition.
 * This will loop through the entire player list (as fetched from <tt>getCompetitionParticipants</tt>),
 * will obtain their current stats, calculate the total xp gained, and update the database with this information
 * using the <tt>updatePlayer</tt> method.
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 */
function updateAllPlayers($conn, $compid) {
	$skill = getSkill ( $conn, $compid );
	$result = getCompetitionParticipants ( $conn, $compid );
	$playercount = $result->num_rows;
	
	for($i = 0; $i < $playercount; $i ++) {
		$playerrow = $result->fetch_assoc ();
		$player = $playerrow ['player'];
		$currentstats = getXpAndLevel ( $player, $skill, true );
		if ($currentstats == null) {
			updatePlayer ( $conn, $playerrow ['id'], 0, 0, 0, 0 );
		} else {
			if($playerrow ['startxp'] == 0)
			{
				initializePlayer($conn, $playerrow ['id'], $currentstats [1], $currentstats [0]);
			}
			else {
			$gainedxp = $currentstats [1] - $playerrow ['startxp'];
			$gainedlvl = $currentstats [0] - $playerrow ['startlvl'];
			
			updatePlayer ( $conn, $playerrow ['id'], $currentstats [1], $currentstats [0], $gainedxp, $gainedlvl );
			}
		}
	}
}
/**
 * Update a specific player's xp gain for a competition.
 *
 * @param $conn -
 *        	Database connection object
 * @param $id -
 *        	The player's ID within the participants table.
 * @param $curxp -
 *        	The player's current xp
 * @param $curlvl -
 *        	The player's current level.
 * @param $gainedxp -
 *        	The player's total gained xp.
 * @param $gainedlvl -
 *        	The player's total gained level.
 * @return boolean - Success Status boolean
 */
function updatePlayer($conn, $id, $curxp, $curlvl, $gainedxp, $gainedlvl) {
	$query = "update participants set endxp = \"" . $curxp . "\", endlvl = \"" . $curlvl . "\", xpgained = \"" . $gainedxp . "\", lvlgained = \"" . $gainedlvl . "\" where id = \"" . $id . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}

/**
 * Updates the 'Update Time' field in the database for a competition.
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 * @param $time -
 *        	The current time
 * @return boolean - Success Status boolean
 */
function setUpdateTime($conn, $compid, $time) {
	$query = "update competitions set updatetime = \"" . $time . "\" where compid=\"" . $compid . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return false;
	} else {
		return true;
	}
}

/**
 * Gets the privacy field value from the database for a competition
 *
 * @param $conn -
 *        	Database connection object
 * @param $compid -
 *        	The ID of the competition
 * @return The privacy value (1 or 0) or null if there is no result
 */
function getPrivacy($conn, $compid) {
	$query = "select privacy from competitions where compid = \"" . $compid . "\"";
	$result = $conn->query ( $query );
	if (! $result) {
		return null;
	} else {
		$row = $result->fetch_assoc ();
		return $row ['privacy'];
	}
}

/**
 * Initiates a background update of user stats for a competition
 *
 * @param $compid -
 *        	The ID of the competition to be updated
 * @param $action -
 *        	The action being done (start, update, add)
 * @return Success Status Boolean
 */
function startBackgroundUpdate($compid, $action) {
	if (! is_numeric ( $compid )) {
		return false;
	}
	if ($action != "start" && $action != "update" && $action != "add") {
		return false;
	}
	$phploc = PHP_BINDIR;
	$command = "bash " . getcwd () . "/comp/runUpdate.sh " . $phploc . "/php " . $action . " " . $compid . " > /dev/null &";
	exec ( $command, $arrayout );
	return true;
}

/**
 * Initiates a background update of user stats for a competition when adding via edit
 *
 * @param $compid -
 *        	The ID of the competition to be updated
 * @param $action -
 *        	The action being done (edit)
 * @param $members -
 *          The array of members being added
 * @return Success Status Boolean
 */
function startBackgroundEdit($compid, $action, $members) {
	if (! is_numeric ( $compid )) {
		return false;
	}
	if ($action != "edit") {
		return false;
	}
	$phploc = PHP_BINDIR;
	$command = "bash " . getcwd () . "/comp/runUpdate.sh " . $phploc . "/php " . $action . " " . $compid;
	foreach ( $members as $newmember ) {
		$command = $command . " " . $newmember;
	}
	$command = $command . " > /dev/null &";
	exec ( $command, $arrayout );
	return true;
}

?>
