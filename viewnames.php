<?php
require("./inc/dbfuncs.php");

$conn = dbconn();
$compid = null;
if(isset($_GET['compid']))
{
	$compid = $_GET['compid'];
}

if($compid != null)
{
	echo "<a href=\"#1k\">Attendance (1k minimum/Skill of the Week)</a><br><a href=\"#50k\">Attendance (50k minimum/Monthly Comp)</a><br><hr><hr>All participants<br><br>";
	$query = "select player from participants where compid=$compid order by player asc";
	$result = $conn->query($query);

	$num_rows = $result->num_rows;
	for($i = 0; $i < $num_rows; $i++)
	{
		$row = $result->fetch_assoc();
		echo $row['player']."<br>";
	}
	
	echo "<a name=\"1k\"></a><hr><hr>Participants qualifying for attendance (1k)<br><br>";
	$query = "select player from participants where compid=$compid and xpgained > 1000 order by player asc";
	$result = $conn->query($query);
	
	$num_rows = $result->num_rows;
	for($i = 0; $i < $num_rows; $i++)
	{
		$row = $result->fetch_assoc();
		echo $row['player']."<br>";
	}
	
	echo "<a name=\"50k\"></a><hr><hr>Participants qualifying for attendance (50k)<br><br>";
	$query = "select player from participants where compid=$compid and xpgained > 50000 order by player asc";
	$result = $conn->query($query);
	
	$num_rows = $result->num_rows;
	for($i = 0; $i < $num_rows; $i++)
	{
		$row = $result->fetch_assoc();
		echo $row['player']."<br>";
	}
}
else 
{
	echo "Competition ID is null";
}
?>