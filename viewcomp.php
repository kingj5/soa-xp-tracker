<?php
date_default_timezone_set('UTC');
session_start();
ini_set("max_execution_time", 0); //TODO - Investigate

include("./inc/templates.php");
require("./inc/dbfuncs.php");
include("./comp/compapi.php");

$footer = template("footer");

$conn = dbconn();
$starttime = null;
$endtime = null;
$status = null;
$currtime = time();
$lastUpdated = null;
$canstart = false;

$cumxp = null;
$avgxp = null;
$cumlvl = null;
$avglvl = null;
$cumxpgained = null;
$avgxpgained = null;
$cumlvlgained = null;
$avglvlgained = null;
$compname = null;

$pagetitle = null;

if(!isset($_GET['compid']))
{
	$progress = false;
	$pagetitle = "Error Viewing Clan Competition";
}
else {
	$progress = true;
	$compid = $_GET['compid'];
	$compname = getCompName($conn, $compid);
	$pagetitle = "Viewing Clan Competition: ".$compname;
}

//Note - leave the below section here - it won't flash white if the background has been set when redirecting.
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $pagetitle; ?></title>
<?= $headerinclude = template("headerinclude");?>

<script src="./template/js/sorttable.js"></script>
</head>
<body id="competitions--view">
<?php

if (isset($_POST['actionButton']))
{
	if (substr($_POST['actionButton'], 0, 6) == "Update" && $progress)
	{
		setCompStatus($conn, 3);
		setUpdateTime($conn, $compid, $currtime);
		startBackgroundUpdate($compid, "update");
		exit_redirect("./viewcomp.php?compid=".$compid);
	}
	else if (substr($_POST['actionButton'], 0, 5) == "Click" && progress)
	{
		setCompStatus($conn, 3);
		setUpdateTime($conn, $compid, $currtime);
		startBackgroundUpdate($compid, "start");
		exit_redirect("./viewcomp.php?compid=".$compid);
	}
}

$checkprivate = getPrivacy($conn, $compid);
$isprivate = "";

if ($checkprivate != null) {
	if ($checkprivate == 1) {
		$isprivate = "competition-status--private";
	}
}

?>

	<?= $header = template("header"); ?>
	<section class="competition-body">
		<h3 class="page-title <?= $isprivate; ?>">Viewing Clan Competition: 
		<?php 
		if(!$progress)
		{
			echo "</h3>You have not selected a competition</h3>";
			echoEndOfPage();
			exit;
		}
		else {
			if ($compname == NULL || $compname == "")
			{
				echo "</h3>You have not selected a valid competition.</h3>";
				echoEndOfPage();
				exit;
			}
			else {
				echo $compname;
			}
		}
		?></h3>
		
			<div class="competition-body__body">
			<div class="competition-body__center">
			Competition Time Frame: <?php getTimeFrame($conn, $compid); ?><br>
			<?php determineTimings ( $conn); ?><br>
			Data Last Updated: <?php getLastUpdate($conn, $compid);?><br>
			<?php displayEditButton(); ?><?php displayUpdateButton();?><br>
			</div>
			<div class="competition-skill--center">
			<?php $skill = getSkill($conn, $compid); getData($conn, $compid); ?>
			<span class="competition-stats--item stats-item--skill">Skill: <b><?= $skill; ?></b></span></div>
			<table class="competition-stats__table">
				<tr class="stats-table__row stats-row--cumulative-xp">
					<td>Cumulative <?= $skill; ?> XP</td>
					<td class="align-right"><?= $cumxp; ?></td>
					<td class="spacer"></td>
					<td>Cumulative <?= $skill; ?> XP Gained</td>
					<td class="align-right"><?= $cumxpgained; ?></td>
				</tr>
				<tr class="stats-table__row stats-row--average-xp">
					<td>Average <?= $skill; ?> XP</td>
					<td class="align-right"><?= $avgxp; ?></td>
					<td class="spacer"></td>
					<td>Average <?= $skill; ?> XP Gained</td>
					<td class="align-right"><?= $avgxpgained; ?></td>
				</tr>
				<tr class="stats-table__row stats-row--cumulative-lvls">
					<td>Cumulative <?= $skill; ?> Levels</td>
					<td class="align-right"><?= $cumlvl; ?></td>
					<td class="spacer"></td>
					<td>Cumulative <?= $skill; ?> Levels Gained</td>
					<td class="align-right"><?= $cumlvlgained; ?></td>
				</tr>
				<tr class="stats-table__row stats-row--average-lvls">
					<td>Average <?= $skill; ?> Levels</td>
					<td class="align-right"><?= $avglvl; ?></td>
					<td class="spacer"></td>
					<td>Average <?= $skill; ?> Levels Gained</td>
					<td class="align-right"><?= $avglvlgained; ?></td>
				</tr>
			</table>
			<div class=competition-body__table><table class="sortable competition-table"><h5 class="competition-table--header">Participants and Stats</h5><tr class="competition-table__header"><th class="table-header__item table-header--rank">Rank</th><th class="table-header__item table-header--username">Username</th><th class="table-header__item table-header--start-exp">Start XP</th><th class="table-header__item table-header--current-exp">Current XP</th><th class="table-header__item table-header--current-lvl">Current Level</th><th class="table-header__item table-header--xp-gain">Gained XP</th><th class="table-header__item table-header--lvl-gain">Gained Levels</th></tr>
			<?= printTable($conn, $compid);?>
			</table>
			</div>
			</div>
		
	</section>

	<?= $footer; ?>

</body>
</html>

<?php 
function echoEndOfPage()
{
	global $footer;
	echo "</section>";
	echo $footer;
	echo "</body></html>";
}

function printTable($conn, $compid)
{
	$result = getCompetitionParticipants($conn, $compid);
	$rows = $result->num_rows;
	for($i = 0; $i < $rows; $i++)
	{
		$row = $result->fetch_assoc();
		$rank = $i + 1;
		$name = $row['player'];
		$namert = str_replace(" ", "+", $name);
		$startxp = number_format($row['startxp']);
		$curxp = number_format($row['endxp']);
		$curlvl = $row['endlvl'];
		$gainedxp = number_format($row['xpgained']);
		$gainedlvl = $row['lvlgained'];
		echo "<tr class=\"competition-table__data\"><td class=\"competition-data__item data-item--rank\">".$rank."</td><td class=\"competition-data__item data-item--name\"><a href=\"http://www.runeclan.com/user/".$namert."\" target=\"_blank\" class=\"competition-data__item-nameLink\">".str_replace("_", " ", ucwords($name))."</a></td><td class=\"competition-data__item data-item--start-xp\">".$startxp."</td><td class=\"competition-data__item data-item--current-xp\">".$curxp."</td><td class=\"competition-data__item data-item--current-lvl\">".$curlvl."</td><td class=\"competition-data__item data-item--gained-xp\">".$gainedxp."</td><td class=\"competition-data__item data-item--gained-lvl\">".$gainedlvl."</td></tr>";
	}
}

function getTimeFrame($conn, $compid)
{
	global $starttime, $endtime, $status;
	$query = "select starttime, endtime, status from competitions where compid = \"".$compid."\"";
	$result = $conn->query($query);
	if($result->num_rows == 1)
	{
		$row = $result->fetch_assoc();
		$startdate = date('m/d/Y H:i', $row['starttime']);
		$starttime = $row['starttime'];
		$enddate = date('m/d/Y H:i', $row['endtime']);
		$endtime = $row['endtime'];
		
		$status = $row['status'];
		echo "<span class=\"data-highlight\">".$startdate." <acronym title=\"Game Time\">UTC</acronym></span> to <span class=\"data-highlight\">".$enddate." <acronym title=\"Game Time\">UTC</acronym></span>";
	}
}

function getLastUpdate($conn, $compid)
{
	global $lastUpdated;
	$query = "select updatetime from competitions where compid = \"".$compid."\"";
	$result = $conn->query($query);
	if($result->num_rows == 1)
	{
		$row = $result->fetch_assoc();
		$updatetime = date('m/d/Y H:i', $row['updatetime']);
		$lastUpdated = $row['updatetime'];
		echo "<span class=\"data-highlight\">".$updatetime." <acronym title=\"Game Time\">UTC</acronym> ".getLastUpdateDiff($lastUpdated)."</span>";
	}
}

function getTimeDiff()
{
	global $endtime;
	$enddate = new DateTime();
	$enddate->setTimestamp($endtime);
	$currdate = new DateTime("now");
	$datediff = $currdate->diff($enddate);
	echo "<span class=\"data-highlight time-to-end\">".$datediff->format("%a days %H hours %I minutes")."</span>";
}

function getLastUpdateDiff($date)
{
	$enddate = new DateTime();
	$enddate->setTimestamp($date);
	$currdate = new DateTime("now");
	$datediff = $currdate->diff($enddate);
	return " (".$datediff->format("%a days %H hours %I minutes")." ago)";
}

function getTimeUntilStart()
{
	global $starttime;
	$startdate = new DateTime();
	$startdate->setTimestamp($starttime);
	$currdate = new DateTime("now");
	$datediff = $currdate->diff($startdate);
	if($datediff->format("%I") == 0)
	{
		echo "<span class=\"data-highlight time-to-start\">".$datediff->format("%a days %H hours 01 minutes")."</span>";
	}
	else {
		echo "<span class=\"data-highlight time-to-start\">".$datediff->format("%a days %H hours %I minutes")."</span>";
	}
}

function setCompStatus($conn, $statuscode)
{
	global $compid, $currtime;
	$query = "update competitions set status = \"".$statuscode."\" where compid = \"".$compid."\"";
	$result = $conn->query($query);
	if(!$result)
	{
		return false;
	}
	else
		return true;
}

function determineTimings($conn) {
	global $starttime, $currtime, $endtime, $status, $canstart;
	if ($status == 2)
	{
		echo "<span class=\"data-highlight comp-ended\">Competition has ended.</span>";
	}
	else if((($starttime <= $currtime and $currtime < $endtime) && $status == 1) || $status == 3)
	{
		echo "Time Left in Competition: ";
		getTimeDiff();
	}
	else if (($starttime <= $currtime and $currtime < $endtime) && $status == 0)
	{
		$canstart = true;
		echo "<span class=\"data-highlight comp-can-start\">Competition can be started; use button below to start.</span>";
	}
	else if (($currtime > $endtime) && ($status == 1 || $status == 0 || $status == 3))
	{
		echo "<span class=\"data-highlight comp-ended\">Competition has ended.</span>";
		$statusChanged = setCompStatus($conn, 2);
		$status = 2;
	}
	else if (($currtime > $endtime) && $status == 2)
	{
		echo "<span class=\"data-highlight comp-ended\">Competition has ended.</span>";
	}
	else if ($status == 2)
	{
		echo "<span class=\"data-highlight comp-ended\">Competition has ended.</span>";
	}
	else if ($currtime < $starttime)
	{
		echo "Time until competition starts: ";
		getTimeUntilStart();
	}
	else 
	{
		echo "Should never get here";
	}
}

function displayUpdateButton()
{
	global $conn, $status, $canstart, $compid, $currtime, $lastUpdated;
	if ($canstart)
	{
		echo "<form action=\"?compid=".$compid."\" method=\"post\"><input type=\"submit\" name=\"actionButton\" value=\"Click to Start Competition\" class=\"competition-action__button start-competition--submit\"></input></form>";
	}
	else if ( $status == 1 && ($currtime - $lastUpdated ) > 300 )
	{
		echo "<form action=\"?compid=".$compid."\" method=\"post\"><input type=\"submit\" name=\"actionButton\" value=\"Update Competition Stats\" class=\"competition-action__button update-competition--submit\"></input></form>";
	}
	else if($status == 3 && ($currtime - $lastUpdated ) > 300 )
	{
		echo "<form action=\"?compid=".$compid."\" method=\"post\"><input type=\"submit\" name=\"actionButton\" value=\"Update Competition Stats\" class=\"competition-action__button update-competition--submit\"></input></form>";
		setCompStatus($conn, 1);
	}
	else if($status == 3)
	{
		echo "<span class=\"competition-error\">Competition Stat Update is in progress.  Check back soon.</span>";
	}
	else if($status == 0 || $status ==2)
	{
		
	}
	else 
	{
		echo "<span class=\"competition-error\">Competition recently updated; competition can only be updated once every 5 minutes.</span>";
	}
}


function displayEditButton()
{
	global $status, $canstart, $compid;
	
	if(isset($_SESSION['is_auth']))
	{	
		if ( $status != 2 )
		{
			echo "<form action=\"editcomp.php?compid=".$compid."\" method=\"post\"><input type=\"submit\" name=\"actionButton\" value=\"Edit Competition\" class=\"competition-action__button update-competition--submit submit-button\"></input></form>";
		}
		echo "<form action=\"viewnames.php?compid=".$compid."\" method=\"post\"><input type=\"submit\" name=\"namesButton\" value=\"Get Participant List\" class=\"competition-action__button update-competition--submit submit-button\"></input></form>";
	}

}


function getData($conn, $compid)
{
	global $cumxp, $avgxp, $cumlvl, $avglvl, $cumxpgained, $avgxpgained, $cumlvlgained, $avglvlgained;
	
	$query = "select sum(endxp) as cumxp from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumxp = $row['cumxp'];
	}
	
	$query = "select sum(endlvl) as cumlvl from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumlvl = $row['cumlvl'];
	}
	
	$query = "select sum(xpgained) as cumxpgained from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumxpgained = $row['cumxpgained'];
	}
	
	$query = "select sum(lvlgained) as cumlvlgained from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$cumlvlgained = $row['cumlvlgained'];
	}
	
	$query = "select count(*) as total from participants where compid=\"".$compid."\"";
	$result = $conn->query($query);
	if($result)
	{
		$row = $result->fetch_assoc();
		$count = $row['total'];
	}
	
	$avgxp = round($cumxp / $count);
	$avglvl = round($cumlvl / $count);
	$avgxpgained = round($cumxpgained / $count);
	$avglvlgained = round($cumlvlgained / $count);
	
	$cumxp = number_format($cumxp);
	$avgxp = number_format($avgxp);
	$cumlvl = number_format($cumlvl);
	$avglvl = number_format($avglvl);
	$cumxpgained = number_format($cumxpgained);
	$avgxpgained = number_format($avgxpgained);
	$cumlvlgained = number_format($cumlvlgained);
	$avglvlgained = number_format($avglvlgained);
}

function exit_redirect($url)
{
	die('<script type="text/javascript">window.location=\''.$url.'\';</script>');
}

?>
